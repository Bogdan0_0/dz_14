#include<iostream>
using namespace std;

class Animal
{
public:

	virtual void Show()
	{
		cout << "Why this Animal?";
	}
};

class Dog : public Animal
{
public:
		void Show() override
	{

		cout << "Woof\n" << endl;
	}
};

class Cat : public Animal
{
public:
	void Show() override
	{
		cout << "Meof\n" << endl;
	}
};

class Wolf : public Animal
{
public:
	void Show() override
	{
		cout << "ayyyyy\n" << endl;
	}
};

int main()
{
	Animal* p = new Dog;
	Animal* p1 = new Cat;
	Animal* p2 = new Wolf;
	p->Show();
}